# wifi-heartbeat

A small utility to keep a Raspberry Pi Zero W reachable on the network

## Overview

The Raspberry Pi 0 appears to disable its wifi radio after some time to conserve power (as discussed on the [Raspberry Pi Forum](https://forums.raspberrypi.com/viewtopic.php?t=259229) and a blog post on [We Work We Play](https://weworkweplay.com/play/rebooting-the-raspberry-pi-when-it-loses-wireless-connection-wifi/))

## crontab

To run the script every 5 minutes add it to crontab

```
crontab -e
```

Assuming the script resides in `/usr/local/bin`

```
*/5 * * * * /usr/bin/sudo -H /usr/local/bin/wifi-heartbeat.sh >> /dev/null 2>&1
```

**Note:** Run this script as sudo which has permission to perform `shutdown`