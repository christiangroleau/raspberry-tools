# see: https://forums.raspberrypi.com/viewtopic.php?t=259229
# see: https://ubuntuforums.org/showthread.php?t=2344219
# see: https://weworkweplay.com/play/rebooting-the-raspberry-pi-when-it-loses-wireless-connection-wifi/

echo "Running wifi heartbeat..."
ping -c5 192.168.11.1 > /dev/null
 
if [ $? != 0 ] 
then
  echo "No network connection. Restarting wlan0."
  /sbin/ifdown 'wlan0'
  # sudo ifconfig wlan0 down
  sleep 5
  /sbin/ifup --force 'wlan0'
  # sudo ifconfig wlan0 up
fi