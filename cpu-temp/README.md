# cpu-temp

A small utility to benchmark a Raspberry Pi CPU temperature

## Getting started
Make sure to have [sysbench](https://github.com/akopytov/sysbench) installed

```
sudo apt install sysbench
```

## Benchmarking CPU temperatures
An intial temperature reading is displayed and again after each prime number calculation

```
./pi-cpu-temp.sh
temp=50.1'C
temp=58.9'C
temp=60.8'C
temp=61.3'C
temp=62.8'C
temp=62.8'C
temp=63.3'C
temp=64.2'C
```

## Credit
This script is from ExplainingComputer's Raspberry Pi 4 heat sync performance tests [video](https://www.youtube.com/watch?v=VJC6OpGpq0Y)